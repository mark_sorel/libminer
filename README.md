
<!-- README.md is generated from README.Rmd. Please edit that file -->

# libminer

<!-- badges: start -->
<!-- badges: end -->

The goal of libminer is to provide an overvirew of your R library setup.
It is a toy package from a workshop and not meant for serious use.

## Installation

You can install the development version of libminer like so:

``` r
devtools::install_gitlab(mark_sorel/libminer)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(libminer)

lib_summary()
#>                                                                    Library
#> 1                                       C:/Program Files/R/R-4.3.3/library
#> 2                        C:/Users/sorelmhs/AppData/Local/R/win-library/4.3
#> 3 C:/Users/sorelmhs/AppData/Local/Temp/RtmpCIOJhD/temp_libpath3f7070da20c9
#>   n_packages
#> 1         30
#> 2        296
#> 3          1

#Or to calculate sizes

lib_summary(sizes = TRUE)
#>                                                                    Library
#> 1                                       C:/Program Files/R/R-4.3.3/library
#> 2                        C:/Users/sorelmhs/AppData/Local/R/win-library/4.3
#> 3 C:/Users/sorelmhs/AppData/Local/Temp/RtmpCIOJhD/temp_libpath3f7070da20c9
#>   n_packages  lib_size
#> 1         30  69170652
#> 2        296 885284519
#> 3          1     16299
```
